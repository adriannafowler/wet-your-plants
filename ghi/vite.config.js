import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react-swc'
import dotenv from 'dotenv'


// https://vitejs.dev/config/
export default defineConfig({
    __VALUE__: `${process.env.VALUE}`,
    plugins: [react()],
    optimizeDeps: true,
    server: {
        watch: {
            usePolling: true,
        },
        host: true,
        strictPort: true,
        port: 3000,
        hmr: true,
    },
});
