import React from 'react'
import Modal from 'react-modal'
import Button from '@mui/material/Button'

Modal.setAppElement('#root')

const LoginModal = ({ message, isOpen, onRequestClose, redirectTo }) => {
    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={onRequestClose}
            contentLabel="Error Login Modal"
            style={{
                overlay: {
                    backgroundColor: 'rgba(0, 0, 0, 0.5)',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                },
                content: {
                    top: '50%',
                    left: '50%',
                    transform: 'translate(-50%, -50%)',
                    width: '30%',
                    height: '18%',
                    backgroundColor: '#cfd6e0',
                    padding: '20px',
                    borderRadius: '8px',
                    boxShadow: '0 0 10px rgba(0, 0, 0, 0.3)',
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'center',
                },
            }}
        >
            <div>
                <h3
                    style={{
                        fontFamily: 'Cupcakie',
                        fontSize: '50px',
                        color: '#79a6a3',
                        marginTop: '0px',
                        marginBottom: '0px',
                    }}
                >
                    {message}
                </h3>
            </div>
            <Button
                style={{
                    fontFamily: 'Cupcakie',
                    fontSize: '24px',
                    fontWeight: 'bold',
                    color: 'white',
                    backgroundColor: '#79a6a3',
                    height: '50px',
                    width: '100%', 
                }}
                onClick={() => onRequestClose(redirectTo)}
            >
                OK
            </Button>
        </Modal>
    )
}

export default LoginModal
