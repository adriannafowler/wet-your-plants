import React, { useState, useEffect, useRef } from 'react'
import { useParams, useNavigate } from 'react-router-dom'
import {
    Card,
    CardActions,
    CardContent,
    Typography,
    Tabs,
    Tab,
    Box,
    List,
    ListItemText,
    IconButton,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from '@mui/material'
import HighlightOffIcon from '@mui/icons-material/HighlightOff'
import EditIcon from '@mui/icons-material/Edit'
import DeleteDialog from './delete_modal'
import EditDialog from './edit_modal'
import './pd.css'
import SideDrawer from '../sidedrawer'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'

function CustomTabPanel({ children, value, index }) {
    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
        >
            {value === index && <Box sx={{ p: 3 }}>{children}</Box>}
        </div>
    )
}
function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    }
}

function PlantDetail() {
    const [newToken, setNewToken] = useState([])
    const [value, setValue] = useState(0)
    const [details, setDetails] = useState({})
    const [todos, setTodos] = useState([])
    const [isEditDialogOpen, setIsEditDialogOpen] = useState(false)
    const navigate = useNavigate()
    const param = useParams()
    const plant_id = param.id

    useEffect(() => {
        const fetchPlantData = async () => {
            try {
                const response = await fetch(
                    `http://localhost:8000/greenhouse/${plant_id}/`,
                    {
                        credentials: 'include',
                    }
                )

                if (response.ok) {
                    const data = await response.json()
                    setDetails(data)
                }
            } catch (error) {
                console.error('Error fetching plant details:', error)
            }
        }

        const fetchTodos = async () => {
            try {
                const response = await fetch(
                    `http://localhost:8000/dashboard?plant_id=${plant_id}`,
                    {
                        credentials: 'include',
                    }
                )

                if (response.ok) {
                    const todosData = await response.json()
                    setTodos(todosData)
                }
            } catch (error) {
                console.error('Error fetching todos:', error)
            }
        }

        const checkToken = () => {
            if (!newToken) {
                navigate('/signin/')
            }
        }

        fetchPlantData()
        fetchTodos()
        checkToken()
    }, [newToken, navigate, plant_id])

    const fetchToken = async () => {
        try {
            const url = `http://localhost:8000/token/`
            const response = await fetch(url, {
                credentials: 'include',
            })

            if (!response.ok) {
                throw new Error('HTTP error!')
            }
            const data = await response.json()
            setNewToken(data.access_token)
        } catch (error) {
            console.error('Error fetching token:', error)
        }
    }

    const handleChange = (event, newValue) => {
        setValue(newValue)
    }

    const DeleteDialogRef = useRef()

    const handleDeleteClick = () => {
        DeleteDialogRef.current.showDialog()
    }

    const handleEditClick = () => {
        setIsEditDialogOpen(true)
    }

    const handleEditDialogClose = () => {
        setIsEditDialogOpen(false)
    }

    const handleSave = () => {}

    return (
        <>
            <div id="header">
                <SideDrawer />
            </div>
            {newToken ? (
                <div className="card-container">
                    <Card
                        id="pd-card"
                        sx={{
                            borderRadius: '20px',
                            width: '1200px',
                            margin: 'auto',
                        }}
                    >
                        <div className="media-content">
                            <div className="image-container">
                                <img
                                    src={details.original_url}
                                    alt="Plant"
                                    style={{
                                        width: '100%',
                                        height: '400px',
                                        objectFit: 'cover',
                                        borderTopLeftRadius: '20px',
                                        borderTopRightRadius: '20px',
                                    }}
                                />
                            </div>
                        </div>
                        <div
                            className="details"
                            sx={{
                                borderTop: 1,
                                borderColor: 'divider',
                                borderBottomLeftRadius: '20px',
                                borderBottomRightRadius: '20px',
                            }}
                        >
                            <div id="details-header">
                                <CardActions>
                                    <Tabs
                                        value={value}
                                        onChange={handleChange}
                                        className="tab-labels"
                                        variant="fullWidth"
                                    >
                                        <Tab
                                            label="Description"
                                            {...a11yProps(0)}
                                            className="tab"
                                        />
                                        <Tab
                                            label="Plant Care"
                                            {...a11yProps(1)}
                                            className="tab"
                                        />
                                        <Tab
                                            label="Care History"
                                            {...a11yProps(2)}
                                            className="tab"
                                        />
                                    </Tabs>
                                </CardActions>
                                <div
                                    className="action-buttons"
                                    id="action-buttons"
                                >
                                    <IconButton onClick={handleDeleteClick}>
                                        <HighlightOffIcon />
                                    </IconButton>
                                    <IconButton onClick={handleEditClick}>
                                        <EditIcon />
                                    </IconButton>
                                </div>
                            </div>
                            <CardContent>
                                <Typography
                                    variant="h5"
                                    component="div"
                                    className="plant_name"
                                    id="common_name"
                                >
                                    {details.common_name}
                                </Typography>
                                <div className="tabs-content">
                                    <CustomTabPanel value={value} index={0}>
                                        {details.description}
                                    </CustomTabPanel>
                                    <CustomTabPanel value={value} index={1}>
                                        <List>
                                            <ListItemText
                                                primary={`Care level: ${details.care_level}`}
                                            />
                                            <ListItemText
                                                primary={`Watering needs: ${details.watering}`}
                                            />
                                            <ListItemText
                                                primary={`Sunlight needs: ${details.sunlight}`}
                                            />
                                            <ListItemText
                                                primary={`Indoor: ${details.indoor}`}
                                            />
                                            <ListItemText
                                                primary={`Watering schedule: ${details.watering_schedule}`}
                                            />
                                        </List>
                                    </CustomTabPanel>
                                    <CustomTabPanel value={value} index={2}>
                                        <TableContainer component={Paper}>
                                            <Table>
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell>
                                                            Task
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            Due date
                                                        </TableCell>
                                                        <TableCell align="right">
                                                            Completed on
                                                        </TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {todos.map((todo) => (
                                                        <TableRow
                                                            key={todo.id}
                                                            sx={{
                                                                '&:last-child td, &:last-child th':
                                                                    {
                                                                        border: 0,
                                                                    },
                                                            }}
                                                        >
                                                            <TableCell
                                                                component="th"
                                                                scope="row"
                                                            >
                                                                {todo.todo}
                                                            </TableCell>
                                                            <TableCell align="right">
                                                                {new Date(
                                                                    todo.due_date +
                                                                        'T00:00'
                                                                ).toLocaleDateString(
                                                                    'en-US',
                                                                    {
                                                                        year: 'numeric',
                                                                        month: '2-digit',
                                                                        day: '2-digit',
                                                                    }
                                                                )}
                                                            </TableCell>
                                                            <TableCell align="right">
                                                                {new Date(
                                                                    todo.time_completed
                                                                ).toLocaleDateString(
                                                                    'en-US',
                                                                    {
                                                                        year: 'numeric',
                                                                        month: '2-digit',
                                                                        day: '2-digit',
                                                                    }
                                                                )}
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>
                                        </TableContainer>
                                    </CustomTabPanel>
                                </div>
                            </CardContent>
                        </div>
                    </Card>
                </div>
            ) : (
                <div>
                    <h1>Please Log In</h1>
                </div>
            )}
            <DeleteDialog ref={DeleteDialogRef} />
            <Dialog open={isEditDialogOpen} onClose={handleEditDialogClose}>
                <DialogTitle>Edit Plant Details</DialogTitle>
                <DialogContent>
                    <EditDialog
                        open={isEditDialogOpen}
                        onClose={handleEditDialogClose}
                        plantid={plant_id}
                        initialData={details}
                        plantId={plant_id}
                        id="edit-dialog"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleEditDialogClose}>Cancel</Button>
                    <Button onClick={handleSave}>Save</Button>
                </DialogActions>
            </Dialog>
        </>
    )
}

export default PlantDetail
