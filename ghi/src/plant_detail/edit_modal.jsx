import React, { useEffect, useState } from 'react'
import Button from '@mui/material/Button'
import TextField from '@mui/material/TextField'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogTitle from '@mui/material/DialogTitle'
import IconButton from '@mui/material/IconButton'
import SearchIcon from '@mui/icons-material/Search'
import { Select, MenuItem, InputLabel, FormControl } from '@mui/material'
import sadPlant from '../public/sad-plant.svg'

const SearchBar = ({ setSearchQuery, onSearch, value }) => (
    <form>
        <TextField
            id="search-bar"
            className="text"
            onChange={(e) => {
                setSearchQuery(e.target.value)
            }}
            variant="outlined"
            placeholder="Search..."
            size="small"
            value={value}
            sx={{
                width: '80%',
                marginRight: '8px',
            }}
        />
        <IconButton type="submit" aria-label="search" onClick={onSearch}>
            <SearchIcon style={{ fill: 'blue' }} />
        </IconButton>
    </form>
)

export default function EditDialog({ open, onClose, plantId, initialData }) {
    const [formData, setFormData] = useState(initialData)
    const [step, setStep] = useState(1)
    const [speciesId, setSpeciesId] = useState(initialData.species_id)
    const [searchResults, setSearchResults] = useState([])
    const [searchQuery, setSearchQuery] = useState(
        initialData.common_name || ''
    )
    const [wateringSchedules, setWateringSchedules] = useState([])

    const fetchWateringSchedules = async () => {
        const url = 'http://localhost:8000/watering-schedules/'
        const response = await fetch(url, {
            credentials: 'include',
        })
        if (response.ok) {
            const data = await response.json()
            setWateringSchedules(data)
        }
    }

    useEffect(() => {
        fetchWateringSchedules()
    }, [])

    const onSearch = async (e) => {
        e.preventDefault()
        if (searchQuery) {
            await getPlantSpecies(searchQuery)
        }
    }

    const getPlantSpecies = async (searchQuery) => {
        const response = await fetch(
            `http://localhost:8000/species_ids/${searchQuery}`,
            { credentials: 'include' }
        )
        if (response.ok) {
            const data = await response.json()
            setSearchResults(data.species)
        }
    }

    const handleSpeciesSelect = (selectedSpecies) => {
        setSpeciesId(selectedSpecies.id)
        setFormData({ ...formData, common_name: selectedSpecies.common_name })
        setStep(2)
    }

    const SearchResultsDropdown = ({ searchResults, onSelect }) => {
        return (
            <div
                style={{
                    display: 'flex',
                    height: '90%',
                    width: '90%',
                    flexWrap: 'wrap',
                    overflowY: 'auto',
                }}
            >
                {searchResults.map((item) => (
                    <div
                        key={item.id}
                        onClick={() => onSelect(item)}
                        style={{ cursor: 'pointer', padding: '10px' }}
                    >
                        <img
                            src={item.original_url || sadPlant}
                            alt={item.common_name}
                            style={{
                                width: '150px',
                                height: '150px',
                                borderRadius: '5px',
                            }}
                        />
                        <div>{item.common_name}</div>
                    </div>
                ))}
            </div>
        )
    }

    useEffect(() => {
        if (searchQuery) {
            getPlantSpecies(searchQuery)
        }
    }, [searchQuery])

    useEffect(() => {
        setFormData(initialData)
        setSearchQuery(initialData.common_name || '')
    }, [initialData])

    const handleChange = (e) => {
        setFormData({ ...formData, [e.target.name]: e.target.value })
    }

    const handleSubmit = async () => {
        try {
            const submitData = {
                name: formData.name,
                source: formData.source,
                species_id: speciesId,
                watering_schedule: formData.watering_schedule,
            }
            const response = await fetch(
                `http://localhost:8000/greenhouse/${plantId}/`,
                {
                    method: 'PUT',
                    headers: { 'Content-Type': 'application/json' },
                    body: JSON.stringify(submitData),
                    credentials: 'include',
                }
            )
            if (!response.ok) {
                throw new Error('Failed to update the plant.')
            }
            onClose()
            window.location.reload()
        } catch (error) {
            console.error('Error:', error)
        }
    }

    return (
        <Dialog
            open={open}
            onClose={onClose}
            maxWidth="md"
            fullWidth={true}
            PaperProps={{
                style: {
                    width: '60%',
                    height: '80%',
                },
            }}
        >
            <DialogTitle>Edit Plant</DialogTitle>
            <DialogContent
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: '16px',
                    height: '100%',
                    width: '100%',
                    overflowX: 'hidden',
                }}
            >
                {step === 1 && (
                    <>
                        <SearchBar
                            setSearchQuery={setSearchQuery}
                            onSearch={onSearch}
                            value={searchQuery}
                        />
                        <SearchResultsDropdown
                            searchResults={searchResults}
                            onSelect={handleSpeciesSelect}
                        />
                    </>
                )}
                {step === 2 && (
                    <>
                        <TextField
                            margin="dense"
                            name="name"
                            label="Nickname"
                            type="text"
                            fullWidth
                            variant="standard"
                            value={formData.name}
                            onChange={handleChange}
                            sx={{
                                width: '90%',
                            }}
                        />
                        <TextField
                            margin="dense"
                            name="source"
                            label="Source - Where did you get your plant from?"
                            type="text"
                            fullWidth
                            variant="standard"
                            value={formData.source}
                            onChange={handleChange}
                            sx={{
                                width: '90%',
                            }}
                        />
                        <FormControl fullWidth margin="dense">
                            <InputLabel id="watering-schedule-label">
                                Watering Schedule
                            </InputLabel>
                            <Select
                                labelId="watering-schedule-label"
                                value={formData.watering_schedule}
                                label="Watering Schedule"
                                sx={{
                                    width: '90%',
                                }}
                                onChange={(e) =>
                                    setFormData({
                                        ...formData,
                                        watering_schedule: e.target.value,
                                    })
                                }
                            >
                                {wateringSchedules.map((schedule) => (
                                    <MenuItem
                                        key={schedule.id}
                                        value={schedule.id}
                                    >
                                        {schedule.schedule}
                                    </MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </>
                )}
            </DialogContent>
            <DialogActions>
                {step === 2 && (
                    <>
                        <Button onClick={() => setStep(1)}>Back</Button>
                        <Button onClick={onClose}>Cancel</Button>
                        <Button onClick={handleSubmit}>Submit</Button>
                    </>
                )}
                {step === 1 && (
                    <>
                        <Button onClick={onClose}>Cancel</Button>
                        <Button onClick={() => setStep(2)}>Next</Button>
                    </>
                )}
            </DialogActions>
        </Dialog>
    )
}
