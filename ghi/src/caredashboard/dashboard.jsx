import React, { useState, useEffect } from 'react'
import {
    Container,
    Grid,
    Paper,
    Button,
    Typography,
    Dialog,
} from '@mui/material'
import WeatherWidget from './weatherwidget'
import DailyTodoList from './dailytodolist'
import AddTodoDialog from '../plant_detail/addtododialog'
import './dashboard.css'
import img6 from '../public/plant_images/img6.png'
import SideDrawer from '../sidedrawer'

const Dashboard = ({ userId }) => {
    const [showDialog, setShowDialog] = useState(false)
    const [todos, setTodos] = useState([])
    const openDialog = () => {
        setShowDialog(true)
    }
    const closeDialog = () => {
        setShowDialog(false)
    }
    const addGreenhouseTask = (newTodo) => {
        setTodos([newTodo, ...todos])
        closeDialog()
    }

    const fetchTodos = async () => {
        try {
            const response = await fetch(`http://localhost:8000/dashboard`, {
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            })
            const data = await response.json()
            setTodos(data)
        } catch (error) {
            console.error('Error fetching todos:', error)
        }
    }

    const event = new Date()
    const options = {
        weekday: 'long',
        year: 'numeric',
        month: 'short',
        day: 'numeric',
    }
    let currentDate = event.toLocaleDateString(undefined, options)

    useEffect(() => {
        fetchTodos()
    }, [])

    useEffect(() => {
        const handleScroll = () => {
            const plantImageContainer = document.getElementById(
                'dashboard-plant-img-container'
            )
            const offset = window.pageYOffset
            plantImageContainer.style.transform = `translateY(${
                offset * 0.5
            }px)`
        }

        window.addEventListener('scroll', handleScroll)

        return () => {
            window.removeEventListener('scroll', handleScroll)
        }
    }, [])

    return (
        <>
            <div id="dashboard-header">
                <div id="sidedrawer-container">
                    <SideDrawer />
                </div>
                <div id="title-container">
                    <Typography id="dashboard-title" variant="h2" gutterBottom>
                        Care Dashboard
                    </Typography>
                </div>
            </div>
            <Container
                maxWidth="lg"
                className="container"
                style={{ marginTop: '20px' }}
            >
                <Grid container spacing={2} justifyContent="center">
                    <Grid item xs={12} md={6}>
                        <Paper
                            elevation={3}
                            style={{
                                padding: '20px',
                                background: '#FFF4E2',
                                borderRadius: '10px',
                            }}
                        >
                            <Typography
                                variant="h4"
                                id="current-date-label"
                                gutterBottom
                            >
                                {currentDate}
                            </Typography>
                            <Button
                                id="add-task-button"
                                variant="contained"
                                color="primary"
                                onClick={openDialog}
                                style={{ marginBottom: '20px' }}
                            >
                                Add task
                            </Button>
                            <DailyTodoList todos={todos} setTodos={setTodos} />
                        </Paper>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Grid id="weather-img-container" container spacing={2}>
                            <div
                                style={{
                                    padding: '10px',
                                    display: 'flex',
                                    flexDirection: 'column',
                                    alignItems: 'center',
                                }}
                            >
                                <WeatherWidget />
                            </div>
                            <Grid item xs={12}>
                                <div
                                    id="dashboard-plant-img-container"
                                    className="fixed-plant-image"
                                    style={{
                                        padding: '10px',
                                        display: 'flex',
                                        flexDirection: 'column',
                                        alignItems: 'center',
                                    }}
                                >
                                    <img src={img6} id="dashboard-plant-img" />
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Dialog
                    open={showDialog}
                    onClose={closeDialog}
                    maxWidth="sm"
                    fullWidth
                >
                    <AddTodoDialog
                        onClose={closeDialog}
                        onAddTodo={addGreenhouseTask}
                    />
                </Dialog>
            </Container>
        </>
    )
}

export default Dashboard
