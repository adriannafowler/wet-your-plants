import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import {
    TextField,
    Button,
    List,
    ListItem,
    ListItemText,
    ListItemSecondaryAction,
    Checkbox,
    IconButton,
    Typography,
    ImageListItem,
    FormControlLabel,
} from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete'
import './dashboard.css'
import checked from '../public/checked.svg'
import unchecked from '../public/unchecked.svg'


const DailyTodoList = ({ onAddTodo }) => {
    const [plants, setPlants] = useState([])
    const [allTodos, setAllTodos] = useState([])
    const [picTodos, setPicTodos] = useState([])
    const [overdueTodos, setOverdueTodos] = useState([])
    const [currentTodos, setCurrentTodos] = useState([])
    const [upcomingTodos, setUpcomingTodos] = useState([])

    const fetchPlants = async () => {
        try {
            const url = `http://localhost:8000/greenhouse/`
            const response = await fetch(url, {
                credentials: 'include',
            })
            if (!response.ok) {
                throw new Error('Failed to fetch plants')
            }
            const data = await response.json()
            setPlants(data)
        } catch (error) {
            console.error('Error fetching plants:', error)
        }
    }

    useEffect(() => {
        fetchPlants()
    }, [])

    const addPics = (todos) => {
        todos.forEach((todo) => {
            const plant = plants.find((plant) => plant.id === todo.plant_id);
            if (plant) {
                todo.img = plant.original_url;
                todo.common_name = plant.common_name;
            }
        });
        setPicTodos([...todos]);
    };

    const formatDueDate = (dueDateStr) => {
        const dueDate = new Date(dueDateStr + "T00:00");
        return dueDate.toLocaleDateString(undefined, { month: 'short', day: 'numeric' });
    };

    const categorizeTodos = (todos) => {
        const now = new Date(new Date().toUTCString());
        now.setHours(0, 0, 0, 0);


        const tomorrow = new Date(now);
        tomorrow.setDate(tomorrow.getDate() + 1);


        const overdue = [];
        const current = [];
        const upcoming = [];

        todos.forEach((todo) => {
            const dueDate = new Date(todo.due_date + "T00:00");

            if (dueDate < now) {
                overdue.push(todo);
            } else if (dueDate.toDateString() === now.toDateString()) {
                current.push(todo);
            } else if (dueDate.toDateString() === tomorrow.toDateString()) {
                upcoming.push(todo);
            }
        });

        setOverdueTodos(overdue.filter(todo => todo.complete === false));
        setCurrentTodos(current);
        setUpcomingTodos(upcoming);
    };


    const fetchAllTodos = async () => {
        try {
            const response = await fetch(`http://localhost:8000/dashboard`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
            })
            const data = await response.json()
            setAllTodos(data)
        } catch (error) {
            console.error('Error fetching todos:', error)
        }
    }

    useEffect(() => {
        fetchAllTodos()
    }, [plants])

    useEffect(() => {
        addPics(allTodos)
        if (picTodos.length > 0) {
            categorizeTodos(picTodos)
        }
    }, [allTodos])

    const toggleTodo = (todoId) => {
        const todoToUpdate = allTodos.find((todo) => todo.id === todoId)
        if (!todoToUpdate) return

        const updatedTodo = {
            ...todoToUpdate,
            complete: !todoToUpdate.complete,
        }

        const response = fetch(
            `http://localhost:8000/dashboard/complete/${todoId}/`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                credentials: 'include',
                body: JSON.stringify({
                    complete: updatedTodo.complete,
                }),
            }
        )
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Failed to update todo completion status')
                }
                return response.json()
            })
            .then((updatedTodoFromServer) => {

                const newTodos = todos.map((todo) =>
                    todo.id === todoId ? updatedTodoFromServer : todo
                )
                setTodos(newTodos)
            })
            .catch((error) => {
                console.error('Error updating todo:', error)
            })
            window.location.reload()
    }

    const removeTodo = async (todoId) => {
        if (!todoId) return
        try {
            const response = await fetch(
                `http://localhost:8000/dashboard/${todoId}/`,
                {
                    method: 'DELETE',
                    headers: { 'Content-Type': 'application/json' },
                    credentials: 'include',
                }
            )

            if (!response.ok) {
                throw new Error('Failed to delete the todo.')
            }

            const newTodos = todos.filter((todo) => todo.id !== todoId)
            setTodos(newTodos)
        } catch (error) {
            console.error('Error:', error)
        }
        window.location.reload()
    }

    return (
        <div className="todo-list">
                <List>
                    {overdueTodos.map((todo) => (
                        <ListItem key={todo.id} className="todo-item" id="overdue-todos">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        id={`custom-checkbox-${todo.id}`}
                                        className="custom-checkbox"
                                        checked={todo.complete}
                                        onClick={() => toggleTodo(todo.id)}
                                        sx={{
                                            width: '32px',
                                            height: '32px',
                                            padding: 0,
                                            margin: 0,
                                            boxSizing: 'border-box',
                                            backgroundImage: `url(${todo.complete ? checked : unchecked}) !important`,
                                            backgroundSize: 'contain',
                                            backgroundPosition: 'center',
                                            backgroundRepeat: 'no-repeat',
                                            '& .MuiCheckbox-root.Mui-checked .MuiSvgIcon-root': {
                                                display: 'none !important',
                                            },
                                            '& .MuiSvgIcon-root': {
                                                display: 'none !important',
                                            },
                                        }}
                                    />
                                }
                                label={
                                    <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
                                        <div style={{ marginRight: '8px', textAlign: 'center' }}>
                                            <img id={`img-${todo.id}`} src={todo.img} alt={todo.common_name} style={{ width: "70px", height: "70px" }} />
                                            <Typography
                                                id={`common_name-${todo.id}`}
                                                className='common-name'
                                                variant="caption"
                                                display="block"
                                                sx={{ color: 'error.main' }}
                                            >
                                                {todo.common_name}
                                            </Typography>
                                        </div>
                                    </div>
                                }
                            />
                            <ListItemText
                                className='(todo.complete ? "complete" : "")'
                                style={{
                                    textDecoration: todo.complete
                                        ? 'line-through'
                                        : 'none',
                                }}
                                sx={{ color: 'error.main' }}
                            >
                                <p id='todo-item1'>{todo.todo}</p>
                            </ListItemText>
                            <ListItemSecondaryAction>
                            <Typography variant="body2" sx={{ marginRight: '16px', display: 'inline-flex', alignItems: 'center', color: 'error.main' }}>
                                {formatDueDate(todo.due_date)}
                            </Typography>
                                <IconButton
                                    edge="end"
                                    className="delete-button"
                                    onClick={() => removeTodo(todo.id)}
                                >
                                    <DeleteIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))}
                    {currentTodos.map((todo) => (
                        <ListItem key={todo.id} className="todo-item">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                    className="custom-checkbox"
                                    checked={todo.complete}
                                    onClick={() => toggleTodo(todo.id)}
                                    sx={{
                                        width: '32px',
                                        height: '32px',
                                        padding: 0,
                                        margin: 0,
                                        boxSizing: 'border-box',
                                        backgroundImage: `url(${todo.complete ? checked : unchecked}) !important`,
                                        backgroundSize: 'contain',
                                        backgroundPosition: 'center',
                                        backgroundRepeat: 'no-repeat',
                                        '& .MuiCheckbox-root.Mui-checked .MuiSvgIcon-root': {
                                            display: 'none !important',
                                        },
                                        '& .MuiSvgIcon-root': {
                                            display: 'none !important',
                                        },
                                    }}
                                />
                                }
                                label={
                                    <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
                                        <div style={{ marginRight: '8px', textAlign: 'center' }}>
                                            <img id='img2' src={todo.img} alt={todo.common_name} style={{ width: "70px", height: "70px" }} />
                                            <Typography id='common_name2' variant="caption" display="block" >{todo.common_name}</Typography>
                                        </div>
                                    </div>
                                }
                            />
                            <ListItemText
                                className='(todo.complete ? "complete" : "")'
                                style={{
                                    textDecoration: todo.complete
                                        ? 'line-through'
                                        : 'none',
                                }}
                            >
                                <p id='todo-item2'>{todo.todo}</p>
                            </ListItemText>
                            <ListItemSecondaryAction>
                                <Typography variant="body2" id='due-date' sx={{ marginRight: '16px', display: 'inline-flex', alignItems: 'center' }}>
                                    {formatDueDate(todo.due_date)}
                                </Typography>
                                <IconButton
                                    edge="end"
                                    className="delete-button"
                                    onClick={() => removeTodo(todo.id)}
                                >
                                    <DeleteIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))}
                </List>
                <List>
                <Typography variant='h5' id='upcoming-label'>
                        Upcoming Tasks
                </Typography>
                    {upcomingTodos.map((todo) => (
                        <ListItem key={todo.id} className="todo-item">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                    className="checkbox"
                                    checked={todo.complete}
                                    onClick={() => toggleTodo(todo.id)}
                                    sx={{
                                        width: '32px',
                                        height: '32px',
                                        padding: 0,
                                        margin: 0,
                                        boxSizing: 'border-box',
                                        backgroundImage: `url(${todo.complete ? checked : unchecked}) !important`,
                                        backgroundSize: 'contain',
                                        backgroundPosition: 'center',
                                        backgroundRepeat: 'no-repeat',
                                        '& .MuiCheckbox-root.Mui-checked .MuiSvgIcon-root': {
                                            display: 'none !important',
                                        },
                                        '& .MuiSvgIcon-root': {
                                            display: 'none !important',
                                        },
                                    }}
                                />
                                }
                                label={
                                    <div style={{ display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}>
                                        <div style={{ marginRight: '8px', textAlign: 'center' }}>
                                            <img id='img3' src={todo.img} alt={todo.common_name} style={{ width: "60px", height: "60px" }} />
                                            <Typography id='common_name3' variant="caption" display="block" >{todo.common_name}</Typography>
                                        </div>
                                    </div>
                                }
                            />
                            <ListItemText
                                className='(todo.complete ? "complete" : "")'
                                style={{
                                    textDecoration: todo.complete
                                        ? 'line-through'
                                        : 'none',
                                }}
                            >
                                <p id='todo-item3' >{todo.todo}</p>
                            </ListItemText>
                            <ListItemSecondaryAction>
                                <Typography variant="body2" id='u-due-date' sx={{ marginRight: '16px', display: 'inline-flex', alignItems: 'center' }}>
                                    {formatDueDate(todo.due_date)}
                                </Typography>
                                <IconButton
                                    edge="end"
                                    className="delete-button"
                                    onClick={() => removeTodo(todo.id)}
                                >
                                    <DeleteIcon />
                                </IconButton>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))}
                </List>
        </div>
    )
}

export default DailyTodoList
