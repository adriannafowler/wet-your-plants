import React, { useState, useEffect } from 'react'
import { Card, CardContent, Typography } from '@mui/material'
import icon01d from './weather-icons/01d.png'
import icon01n from './weather-icons/01n.png'
import icon02d from './weather-icons/02d.png'
import icon02n from './weather-icons/02n.png'
import icon03d from './weather-icons/03d.png'
import icon03n from './weather-icons/03n.png'
import icon04d from './weather-icons/04d.png'
import icon04n from './weather-icons/04n.png'
import icon09d from './weather-icons/09d.png'
import icon09n from './weather-icons/09n.png'
import icon10d from './weather-icons/10d.png'
import icon10n from './weather-icons/10n.png'
import icon11d from './weather-icons/11d.png'
import icon11n from './weather-icons/11n.png'
import icon13d from './weather-icons/13d.png'
import icon13n from './weather-icons/13n.png'
import icon50d from './weather-icons/50d.png'
import icon50n from './weather-icons/50n.png'

const iconMap = {
    '01d': icon01d,
    '01n': icon01n,
    '02d': icon02d,
    '02n': icon02n,
    '03d': icon03d,
    '03n': icon03n,
    '04d': icon04d,
    '04n': icon04n,
    '09d': icon09d,
    '09n': icon09n,
    '10d': icon10d,
    '10n': icon10n,
    '11d': icon11d,
    '11n': icon11n,
    '13d': icon13d,
    '13n': icon13n,
    '50d': icon50d,
    '50n': icon50n,
}

const WeatherWidget = () => {
    const [zipcode, setZipcode] = useState('')
    const [newToken, setNewToken] = useState([])
    const [weather, setWeather] = useState({})

    const fetchTokenOrRedirect = async () => {
        try {
            const url = `http://localhost:8000/token/`
            const response = await fetch(url, {
                credentials: 'include',
            })

            if (!response.ok) {
                throw new Error('HTTP error!')
            }
            const data = await response.json()
            const zipcode = data.account.zipcode
            setZipcode(zipcode)
            setNewToken(data.access_token)
        } catch (error) {
            console.error('Error fetching token:', error)
            setModalOpen(true)
        }
    }

    useEffect(() => {
        fetchTokenOrRedirect()
    }, [])

    const fetchWeather = async () => {
        try {
            const url = `http://localhost:8000/weather/${zipcode}`
            const response = await fetch(url, {
                credentials: 'include',
            })
            if (!response.ok) {
                throw new Error('Failed to fetch weather')
            }
            const data = await response.json()
            setWeather(data)
        } catch (error) {
            console.error('Error fetching weather:', error)
        }
    }

    useEffect(() => {
        if (zipcode) {
            fetchWeather()
        }
    }, [zipcode])

    return (
        <Card id="weather-card">
            <CardContent id="weather-card-content">
                <>
                    <Typography
                        variant="h6"
                        id="weather-heading"
                    >{`Weather in ${weather.name} County`}</Typography>
                    <img
                        src={iconMap[weather.icon]}
                        alt="Weather icon"
                        id="weather-icon"
                    />
                    <Typography id="temp">{`${weather.temp} °F`}</Typography>
                    <Typography id="weather-description">{`${weather.description}`}</Typography>
                </>
            </CardContent>
        </Card>
    )
}

export default WeatherWidget
