import * as React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import { Grid, Paper, Typography } from '@mui/material';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import Header from './header';
import Footer from './footer';
import './background.css'
import greenhouse2 from '../public/greenhouse.png'
import FeaturedPlantImages from './featured_plants';
import care_dashboard from '../public/care-dashboard.svg'


const sections = [

];


const defaultTheme = createTheme();

export default function HomePage() {
  return (
    <ThemeProvider theme={defaultTheme}>
      <CssBaseline />
      <Container maxWidth="lg">
        <Header title="Wet Your Plants" sections={sections} />
        <main>
          <div className='plant_img_container'>
            <h3 className='featured_plants_title'>
              Featured Plants
            </h3>
            <FeaturedPlantImages className='featured_plants' />
          </div>
          <div style={{ flexGrow: 1, margin: 30 }} >
            <Grid container spacing={2} id="features-container">
              {/* First Row */}
              <Grid item xs={6} style={{ marginBottom: '75px' }}>
                <Paper elevation={3} style={{ padding: 20 }} id="img-container">
                  <img src={greenhouse2} alt="Description" style={{ width: '100%' }} id="gh2-img" />
                </Paper>
              </Grid>
              <Grid item xs={6} textAlign="center">
                <Typography variant="h2" id="gh-title">
                  Greenhouse
                </Typography>
                <Typography id="gh-description" >
                  Keep you plant collection visually organized. No plant left behind!
                </Typography>
              </Grid>
              {/* Second Row */}
              <Grid item xs={6} textAlign="center">
                <Typography variant="h2" id="cd-title">
                  Care Dashboard
                </Typography>
                <Typography id="cd-description">
                  Stay on top of all your plant care tasks!
                </Typography>
              </Grid>
              <Grid item xs={6}>
                <Paper elevation={3} style={{ padding: 20 }} id="img-container2">
                  <img src={care_dashboard} alt="Description" style={{ width: '100%' }} id="cd-img"/>
                </Paper>
              </Grid>
            </Grid>
          </div>
        </main>
      </Container>
      <Footer
        title="About Us"
        description="Wetting plants since 1999"
      />
    </ThemeProvider>
  );
}
