from acls import get_weather_data


class WeatherRepository:
    def get_all(self, query: str):
        return get_weather_data(query)
