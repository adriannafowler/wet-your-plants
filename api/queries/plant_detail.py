import logging
from typing import Optional
from queries.pool import pool
from models import PlantIn, PlantOut
from acls import get_plant_details


class PlantRepository:
    def get_one(self, plant_id: int) -> Optional[PlantOut]:
        """
        Retrieves a single plant record by plant ID.

        Parameters
        ----------
        plant_id : int
            The ID of the plant to retrieve.

        Returns
        -------
        Optional[PlantOut]
            A PlantOut object representing the plant if found, otherwise None.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT *
                        FROM plants
                        WHERE id = %s;
                        """,
                        [plant_id],
                    )
                    record = result.fetchone()
                    return self.record_out(record)
        except Exception as e:
            logging.error("Error in creating plant:", e)
            raise

    def delete(self, plant_id: int) -> bool:
        """
        Deletes a plant record identified by the provided plant_id.


        Parameters
        ----------
        plant_id : int
            The ID of the plant record to be deleted.

        Returns
        -------
        bool
            Returns True if the plant record is successfully deleted, False otherwise.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM plants
                        WHERE id = %s
                        """,
                        [plant_id],
                    )
                    return True
        except Exception as e:
            logging.error("Error in creating plant: %s", e)
            raise

    def update(self, plant_id: int, plant: PlantIn) -> PlantOut:
        """
        Updates an existing plant record with new information.

        Parameters
        ----------
        plant_id : int
            The ID of the plant to be updated.
        plant : PlantIn
            An instance of PlantIn containing the updated plant information.

        Returns
        -------
        PlantOut
            A PlantOut object representing the updated plant record.
        """
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                    """
                    SELECT species_id, owner_id
                    FROM plants
                    WHERE id = %s;
                    """,
                    [plant_id],
                    )
                    current_data = db.fetchone()
                    print("CURRENT DATA:", current_data)
                    if current_data is None:
                        raise ValueError("Plant not found")

                    current_species_id, current_owner_id = current_data

                    if plant.species_id != current_species_id:
                        details = get_plant_details(plant.species_id)
                    else:
                        details = None

                    db.execute(
                        """
                        UPDATE plants
                        SET name = %s
                            , source = %s
                            , common_name = COALESCE(%s, common_name)
                            , type = COALESCE(%s, type)
                            , cycle = COALESCE(%s, cycle)
                            , watering = COALESCE(%s, watering)
                            , sunlight = COALESCE(%s, sunlight)
                            , indoor = COALESCE(%s, indoor)
                            , care_level = COALESCE(%s, care_level)
                            , maintenance = COALESCE(%s, maintenance)
                            , description = COALESCE(%s, description)
                            , hardiness = COALESCE(%s, hardiness)
                            , original_url = COALESCE(%s, original_url)
                            , dimensions = COALESCE(%s, dimensions)
                            , species_id = %s
                            , watering_schedule = %s
                        WHERE id = %s
                        RETURNING id, name, source, common_name, type, cycle, watering, sunlight,
                            indoor, care_level, maintenance, description, hardiness, original_url,
                            dimensions, species_id, owner_id, watering_schedule;
                        """,
                        [
                            plant.name,
                            plant.source,
                            details["common_name"] if details else None,
                            details["type"] if details else None,
                            details["cycle"] if details else None,
                            details["watering"] if details else None,
                            details["sunlight"] if details else None,
                            details["indoor"] if details else None,
                            details["care_level"] if details else None,
                            details["maintenance"] if details else None,
                            details["description"] if details else None,
                            details["hardiness"] if details else None,
                            details["original_url"] if details else None,
                            details["dimensions"] if details else None,
                            plant.species_id,
                            plant.watering_schedule,
                            plant_id,
                        ],
                    )
                    if db.rowcount == 0:
                        raise ValueError(
                            "No updates made, plant data may be identical or plant not found"
                        )
                    record = db.fetchone()
                    return self.record_out(record)
        except Exception as e:
            logging.error("Error in updating plant: %s", e)
            raise

    def record_out(self, record):
        """
        Converts a database record into a PlantOut object.

        Parameters
        ----------
        record : tuple
            A tuple representing a row from the plants database table.

        Returns
        -------
        PlantOut
            A PlantOut object representing the plant item.
        """
        plant_dict = {
        "id": record[0],
        "name": record[1],
        "source": record[2],
        "common_name": record[3],
        "type": record[4],
        "cycle": record[5],
        "watering": record[6],
        "sunlight": record[7],
        "indoor": record[8],
        "care_level": record[9],
        "maintenance": record[10],
        "description": record[11],
        "hardiness": record[12],
        "original_url": record[13],
        "dimensions": record[14],
        "species_id": record[15],  # Include species_id here
        "owner_id": record[16],
        "watering_schedule": record[17],
        }
        return PlantOut(**plant_dict)
