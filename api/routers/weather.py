from fastapi import APIRouter, Depends
from queries.weather import WeatherRepository
from models import UserOut
from authenticator import authenticator

router = APIRouter()


@router.get("/weather/{query}")
def get_all_species_id(
    query: str,
    repo: WeatherRepository = Depends(),
    user: UserOut = Depends(authenticator.get_current_account_data),
):
    return repo.get_all(query)
