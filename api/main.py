from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import (
    plant_detail,
    users,
    watering_schedules,
    greenhouse,
    dashboard,
    get_plant_species,
    weather
)
from authenticator import authenticator

app = FastAPI()

origins = [
    "https://the-horticoders.gitlab.io/wet-your-plants/",
    "http://localhost:3000"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "You hit the root path!"}


app.include_router(authenticator.router)
app.include_router(plant_detail.router)
app.include_router(users.router)
app.include_router(watering_schedules.router)
app.include_router(greenhouse.router)
app.include_router(dashboard.router)
app.include_router(get_plant_species.router)
app.include_router(weather.router)
